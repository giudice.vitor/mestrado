function plotNonlinearSimulation()

footDimensions.length = 0.055;
footDimensions.width = 0.025;
plotter = WalkingPlotter(footDimensions);
idx = 1:600;
time = load('time.txt');
time = time(idx);
com = load('com.txt');
support = load('support_foot.txt');
% zmp = load('cop.txt');
% imu = load('imu.txt');
% zmp(:,1) = com(:,1) - imu(:,1) * (0.215 / 9.81);
% zmp(:,2) = com(:,4) - imu(:,2) * (0.215 / 9.81);
zmp = load('zmp.txt');
figure;
plotter.plotZMPTracking(support(idx,1), support(idx,2), support(idx,3),...
    [], [], com(idx,1), com(idx,4), movmean(zmp(idx,1),1), movmean(zmp(idx,2),1));

figure;
dxCoM = com(idx, 2);
dxCoMPred = com(idx, 7);
plotter.plotTimeSeries(time, dxCoM, dxCoMPred, 'Time (s)', 'X CoM Speed (m/s)', []);
plot(time, movmean(dxCoM, 50), '--g', 'LineWidth', 2);
legend('Actual', 'Predicted', 'Actual - Moving Mean')
figure;
dyCoM = com(idx, 5);
dyCoMPred = com(idx, 8);
plotter.plotTimeSeries(time, dyCoM, dyCoMPred, 'Time (s)', 'Y CoM Speed (m/s)', []);
legend('Actual', 'Predicted')
% figure;
% xCoM = com(idx, 1);
% xCoMPred = com(idx, 10);
% plotter.plotTimeSeries(time, xCoM, xCoMPred, 'Time (s)', 'X CoM (m)', []);
% legend('Actual', 'Predicted');
% figure;
% yCoM = com(idx, 4);
% yCoMPred = com(idx, 11);
% plotter.plotTimeSeries(time, yCoM, yCoMPred, 'Time (s)', 'Y CoM (m)', []);
% legend('Actual', 'Predicted');
% figure;
% hold on;
% plot(xCoM, yCoM, 'LineWidth', 2);
% plot(xCoMPred, yCoMPred, 'r', 'LineWidth', 2);
% xlabel('X CoM');
% ylabel('Y CoM');
% legend('Actual', 'Predicted');
% comError = load('fixed_com_error.txt');
% figure;
% plotter.plotTimeSeries(time, com(idx, 1), com(idx, 1) + comError(idx, 1), 'Time (s)', 'X CoM (m)', []);
% legend('Fixed CoM', 'Real CoM');
% figure;
% plotter.plotTimeSeries(time, com(idx, 4), com(idx, 4) + comError(idx, 2), 'Time (s)', 'Y CoM (m)', []);
% legend('Fixed CoM', 'Real CoM');
figure;
plotter.plotTimeSeries(time, support(idx, 1), support(idx, 2), 'Time (s)', 'Support foot position (m)', []);
legend('x support', 'y support');
% nextSwingFoot = load('next_swing_foot.txt');
% nextSwingFootX = nextSwingFoot(idx, 1);
% swingFootX = nextSwingFoot(idx, 4);
% nextSwingFootY = nextSwingFoot(idx, 2);
% swingFootY = nextSwingFoot(idx, 5);
% swingFootFKModelX = nextSwingFoot(idx, 7);
% swingFootFKModelY = nextSwingFoot(idx, 8);
% figure;
% plotter.plotTimeSeries(time, swingFootX, nextSwingFootX, 'Time (s)', 'X Swing Foot (m)', []);
% plot(time, swingFootFKModelX, 'g--', 'LineWidth', 2);
% legend('Actual', 'Predicted', 'FK Model')
% figure;
% plotter.plotTimeSeries(time, swingFootY, nextSwingFootY, 'Time (s)', 'Y Swing Foot (m)', []);
% plot(time, swingFootFKModelY, 'g--', 'LineWidth', 2);
% legend('Actual', 'Predicted', 'FK Model')
% subplot(2, 1, 2);
% dyCoMMiddle = comMiddle(idx, 5);
% dyCoMEfficient = comEfficient(idx, 5);
% plotter.plotTimeSeries(time, dyCoMMiddle, dyCoMEfficient, 'Time (s)', 'Y Speed (m/s)', []);
% legend('Classical', 'ZMP Manipulation')

end
