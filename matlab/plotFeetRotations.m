function plotFeetRotations(theta)

l = 0.06;
w = 0.03;

figure;
hold on;

for s=1:length(theta)
    psi = theta(s);
    center = [0; 0];
    R = rotationMatrix(psi);
    topLeft = center + R * [-l / 2; w / 2];
    topRight = center + R * [l / 2; w / 2];
    bottomLeft = center + R * [-l / 2; -w / 2];
    bottomRight = center + R * [l / 2; -w / 2];
    plot([topLeft(1), topRight(1), bottomRight(1), bottomLeft(1), topLeft(1), topRight(1)],...
        [topLeft(2), topRight(2), bottomRight(2), bottomLeft(2), topLeft(2), topRight(2)],...
        'Color', [0.5 0.5 0.5], 'LineWidth', 2);
end

axis equal
axis off

end