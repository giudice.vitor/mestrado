classdef YalmipOptimizationFactory < handle
    methods(Static)
        function variables = makeHumanoidsVariables(N, Nf)
            Nbu = YalmipOptimizationFactory.getNbu(N, Nf);
            variables.N = N;
            variables.Nf = Nf;
            variables.Nbu = Nbu;
            variables.d3X = sdpvar(N, 1);
            variables.Xfb = sdpvar(Nf, 1);
            variables.d3Y = sdpvar(N, 1);
            variables.Yfb = sdpvar(Nf, 1);
            variables.Xfaux = sdpvar(N, 1);
            variables.Yfaux = sdpvar(N, 1);
            variables.Sr = sdpvar(Nf, 1);
            variables.Bu = binvar(Nbu, 1);
            variables.Ub = YalmipOptimizationFactory.makeUb(variables.Bu, N, Nf);
            variables.Bs = binvar(Nf, 1);
        end
        
        function variables = makeRotationDecisionVariables(N, Nf)
            Nbu = YalmipOptimizationFactory.getNbu(N, Nf);
            variables.N = N;
            variables.Nf = Nf;
            variables.Nbu = Nbu;
            variables.d3X = sdpvar(N, 1);
            variables.Xfb = sdpvar(Nf, 1);
            variables.d3Y = sdpvar(N, 1);
            variables.Yfb = sdpvar(Nf, 1);
            variables.Xfaux = sdpvar(N, 1);
            variables.Yfaux = sdpvar(N, 1);
            variables.Sr = sdpvar(Nf, 1);
            variables.Bu = binvar(Nbu, 1);
            variables.Ub = YalmipOptimizationFactory.makeUb(variables.Bu, N, Nf);
            variables.Bs = binvar(Nf, 1);
        end
        
        function Nbu = getNbu(N, Nf)
            Nbu = Nf * (Nf + 1) / 2 + (N - Nf) * Nf;
        end
        
        function cost = makeJerkCost(variables, alpha)
            cost = (alpha / 2) * variables.d3X' * variables.d3X +...
                (alpha / 2) * variables.d3Y' * variables.d3Y;
        end
        
        function cost = makeVelocityTrackingCost(variables, prediction, dXref, dYref, x, y, beta)
            velocityErrorX = dXref - (prediction.Pvs * x + prediction.Pvu * variables.d3X);
            velocityErrorY = dYref - (prediction.Pvs * y + prediction.Pvu * variables.d3Y);
            cost = (beta / 2) * (velocityErrorX' * velocityErrorX) +...
                (beta / 2) * (velocityErrorY' * velocityErrorY);
        end
        
        function cost = makeZMPTrackingCost(variables, prediction, x, y, gamma)
            zmpErrorX = variables.Xfaux - (prediction.Pzs * x + prediction.Pzu * variables.d3X);
            zmpErrorY = variables.Yfaux - (prediction.Pzs * y + prediction.Pzu * variables.d3Y);
            cost = (gamma / 2) * (zmpErrorX' * zmpErrorX) +...
                (gamma / 2) * (zmpErrorY' * zmpErrorY);
        end
        
        function cost = makeStepDurationCost(variables, delta)
            N = variables.N;
            Nf = variables.Nf;
            for j=1:Nf
                S(j, 1) = sum(variables.Ub(j:N, j));
            end
            cost = (delta / 2) * ((variables.Sr - S)' * (variables.Sr - S));
        end
        
        function [dxfr, dyfr, bfr] = makeFootReachability(ySep, theta)
            dxfr = cell(1, length(theta));
            dyfr = cell(1, length(theta));
            
            for i=1:length(theta)
                dxfr{i} = -sin(theta(i));
                dyfr{i} = cos(theta(i));
            end
            
            bfr = -ySep;
        end
        
        function [dxvl, dyvl, bvl] = makeVelocityLimits(maxVelocity, theta)
            vxmax = maxVelocity.vxmax;
            vymax = maxVelocity.vymax;
            
            dxvl = cell(1, length(theta));
            dyvl = cell(1, length(theta));
            
            for i=1:length(theta)
                dxvl{i} = [cos(theta(i)); -cos(theta(i)); -sin(theta(i)); sin(theta(i))];
                dyvl{i} = [sin(theta(i)); -sin(theta(i)); cos(theta(i)); -cos(theta(i))];
            end
            
            bvl = [vxmax; vxmax; vymax; vymax];
        end
        
        function [dxa, dya, ba] = makeAccelerationLimits(maxAcceleration, theta)
            axmax = maxAcceleration.axmax;
            aymax = maxAcceleration.aymax;
            
            dxa = cell(1, length(theta));
            dya = cell(1, length(theta));
            
            for i=1:length(theta)
                dxa{i} = [cos(theta(i)); -cos(theta(i)); -sin(theta(i)); sin(theta(i))];
                dya{i} = [sin(theta(i)); -sin(theta(i)); cos(theta(i)); -cos(theta(i))];
            end
            
            ba = [axmax; axmax; aymax; aymax];
        end
        
        function [dxsp, dysp, bsp] = makeSupportPolygon(footDimensions, theta)
            dxsp = cell(1, length(theta));
            dysp = cell(1, length(theta));
            
            for i=1:length(theta)
                dxsp{i} = [cos(theta(i)); -cos(theta(i)); -sin(theta(i)); sin(theta(i))];
                dysp{i} = [sin(theta(i)); -sin(theta(i)); cos(theta(i)); -cos(theta(i))];
            end
            
            l = footDimensions.length;
            w = footDimensions.width;
            bsp = [l/2; l/2; w/2; w/2];
        end
        
        function constraints = makeIntegerConstraints(variables)
            Nf = variables.Nf;
            N = variables.N;
            o = Nf + 10;
            constraints = [variables.Ub(1, 1) == 1];
            for i=1:Nf-1
                constraints = constraints +...
                    [(1:i+1) * variables.Ub(i+1, 1:i+1)' - (1:i) * variables.Ub(i, 1:i)' >= 0];
                constraints = constraints +...
                    [(o+1:o+i+1) * variables.Ub(i+1, 1:i+1)' - (o+1:o+i) * variables.Ub(i, 1:i)' <= 1];
            end
            for i=Nf:N-1
                constraints = constraints +...
                    [(1:Nf) * variables.Ub(i+1, 1:Nf)' - (1:Nf) * variables.Ub(i, 1:Nf)' >= 0];
                constraints = constraints +...
                    [(1:Nf) * variables.Ub(i+1, 1:Nf)' - (1:Nf) * variables.Ub(i, 1:Nf)' <= 1];
            end
        end
        
        function constraints = makeAuxiliaryVariablesConstraints(variables)
            N = variables.N;
            Nf = variables.Nf;
            constraints = [];
            for j=1:Nf
                for i=j:Nf
                    constraints = constraints +...
                        implies(variables.Ub(i, j), variables.Xfaux(i) == variables.Xfb(j));
                    constraints = constraints +...
                        implies(variables.Ub(i, j), variables.Yfaux(i) == variables.Yfb(j));
                end
                for i=Nf+1:N
                    constraints = constraints +...
                        implies(variables.Ub(i, j), variables.Xfaux(i) == variables.Xfb(j));
                    constraints = constraints +...
                        implies(variables.Ub(i, j), variables.Yfaux(i) == variables.Yfb(j));
                end
            end
        end
        
        function constraints = makeZMPErrorConstraints(variables, prediction,...
                dxsp, dysp, bsp, x, y)
            N = variables.N;
            Nf = variables.Nf;
            constraints = [];
            Zx = prediction.Pzs * x + prediction.Pzu * variables.d3X;
            Zy = prediction.Pzs * y + prediction.Pzu * variables.d3Y;
            for j=1:Nf
                for i=j:Nf
                    constraints = constraints +...
                        implies(variables.Ub(i, j),...
                        [dxsp{j}, dysp{j}] * [Zx(i) - variables.Xfb(j); Zy(i) - variables.Yfb(j)] <= bsp);
                end
                for i=Nf+1:N
                    constraints = constraints +...
                        implies(variables.Ub(i, j),...
                        [dxsp{j}, dysp{j}] * [Zx(i) - variables.Xfb(j); Zy(i) - variables.Yfb(j)] <= bsp);
                end
            end
        end
        
        function constraints = makeFeetCollisionConstraints(variables,...
                dxfr, dyfr, bfr, isLeftSwing)
            Nf = variables.Nf;
            constraints = [];
            for j=1:Nf-1
                constraints = constraints +...
                    [(-1)^(j + isLeftSwing - 1) * [dxfr{j}, dyfr{j}] * [variables.Xfb(j+1) - variables.Xfb(j); variables.Yfb(j+1) - variables.Yfb(j)] <= bfr];
            end
        end
        
        function constraints = makeJointsConstraints(variables,...
                dxvl, dyvl, bvl, xa, ya, T)
            N = variables.N;
            Nf = variables.Nf;
            for j=1:Nf
                S(j, 1) = sum(variables.Ub(j:N, j));
            end
            constraints = [[dxvl{1}, dyvl{1}] * [variables.Xfb(2) - xa; variables.Yfb(2) - ya] <= S(1) * T * bvl];
            for j=2:Nf-1
                constraints = constraints +...
                    [[dxvl{j}, dyvl{j}] * [variables.Xfb(j+1) - variables.Xfb(j-1); variables.Yfb(j+1) - variables.Yfb(j-1)] <= S(j) * T * bvl];
            end
        end
        
        function constraints = makeCurrentStepConstraints(variables, xc, yc)
            constraints = [variables.Xfb(1) == xc, variables.Yfb(1) == yc];
        end
        
        function constraints = makeAccelerationConstraints(variables, prediction,...
                dxa, dya, ba, x, y)
            N = variables.N;
            Nf = variables.Nf;
            d2X = prediction.Pas * x + prediction.Pau * variables.d3X;
            d2Y = prediction.Pas * y + prediction.Pau * variables.d3Y;
            constraints = [];
            for j=1:Nf
                for i=j:Nf
                    constraints = constraints +...
                        implies(variables.Ub(i, j),...
                        [[dxa{j}, dya{j}] * [d2X(i); d2Y(i)] <= ba]);
                end
                for i=Nf+1:N
                    constraints = constraints +...
                        implies(variables.Ub(i, j),...
                        [[dxa{j}, dya{j}] * [d2X(i); d2Y(i)] <= ba]);
                end
            end
        end
        
        function constraints = makeStepDurationReferenceContinuousConstraints(variables,...
                nref, kprime)
            N = variables.N;
            Nf = variables.Nf;
            
            nref1 = nref - kprime;
            nrefVector = [nref1; nref * ones(Nf-1, 1)];
            constraints = [variables.Sr(1) == nrefVector(1)];
            for j=2:Nf
                constraints = constraints +...
                    implies(variables.Bs(j),...
                    [variables.Sr(j) == nrefVector(j)]);
                sigma = sum(variables.Ub(:, 1));
                for k=2:j-1
                    sigma = sigma + sum(variables.Ub(k:end, k));
                end
                constraints = constraints +...
                    implies(~variables.Bs(j),...
                    [variables.Sr(j) == N - sigma]);
            end
        end
        
        function constraints = makeStepDurationReferenceBinaryConstraints(variables,...
                nref, kprime)
            N = variables.N;
            Nf = variables.Nf;
            
            nref1 = nref - kprime;
            nrefVector = [nref1; nref * ones(Nf-1, 1)];
            constraints = [variables.Bs(1) == 1];
            for j=2:Nf
                sigma = sum(variables.Ub(:, 1));
                for k=2:j-1
                    sigma = sigma + sum(variables.Ub(k:end, k));
                end
                constraints = constraints +...
                    [(N - nrefVector(j)) * variables.Bs(j) >= N - sigma - nrefVector(j),...
                    nrefVector(j) * variables.Bs(j) <= N - sigma];
            end
        end
        
        function constraints = makeBoundConstraints(variables)
%             variables.d3X = sdpvar(N, 1);
%             variables.Xfb = sdpvar(Nf, 1);
%             variables.d3Y = sdpvar(N, 1);
%             variables.Yfb = sdpvar(Nf, 1);
%             variables.Xfaux = sdpvar(N, 1);
%             variables.Yfaux = sdpvar(N, 1);
%             variables.Sr = sdpvar(Nf, 1);
%             variables.Bu = binvar(Nbu, 1);
            N = variables.N;
            Nf = variables.Nf;
            constraints = [-1000 * ones(N, 1) <= variables.d3X <= 1000 * ones(N, 1),...
                -10 * ones(Nf, 1) <= variables.Xfb <= 10 * ones(Nf, 1),...
                -1000 * ones(N, 1) <= variables.d3Y <= 1000 * ones(N, 1),...
                -10 * ones(Nf, 1) <= variables.Yfb <= 10 * ones(Nf, 1),...
                -10 * ones(N, 1) <= variables.Xfaux <= 10 * ones(N, 1),...
                -10 * ones(N, 1) <= variables.Yfaux <= 10 * ones(N, 1),...
                zeros(Nf, 1) <= variables.Sr <= N * ones(Nf, 1)];
        end
        
        function Ub = makeUb(Bu, N, Nf)
            for j=1:Nf
                for i=1:Nf
                    Ub(i, j) = Bu(i*(i-1)/2+j);
                end
                for i=Nf+1:N
                    Ub(i, j) = Bu(Nf*(Nf+1)/2+(i-Nf-1)*Nf+j);
                end
            end
        end
    end
end