function plotNadaptedDifference(simulation, simulationNadapted)

fontSize = 14;

times = (simulation.T / simulation.Ncon) * (1:simulation.Nsim)';
figure;
hold on;
grid on;
plot(times, simulation.simulation.Zxh, 'LineWidth', 2, 'Color', 'b');
plot(times, simulationNadapted.simulation.Zxh, 'LineWidth', 2, 'Color', 'r');
xlabel('Time (s)', 'FontSize', fontSize, 'interpreter', 'latex');
ylabel('X ZMP (m)', 'FontSize', fontSize, 'interpreter', 'latex');
set(gca, 'FontSize', fontSize);
set(gca, 'TickLabelInterpreter', 'latex');
legend({'Fixed $N$', 'Adaptive $N$'}, 'FontSize', fontSize, 'interpreter', 'latex');

figure;
hold on;
grid on;
plot(times, simulation.simulation.Zyh, 'LineWidth', 2, 'Color', 'b');
plot(times, simulationNadapted.simulation.Zyh, 'LineWidth', 2, 'Color', 'r');
xlabel('Time (s)', 'FontSize', fontSize, 'interpreter', 'latex');
ylabel('Y ZMP (m)', 'FontSize', fontSize, 'interpreter', 'latex');
set(gca, 'FontSize', fontSize);
set(gca, 'TickLabelInterpreter', 'latex');
legend({'Fixed $N$', 'Adaptive $N$'}, 'FontSize', fontSize, 'interpreter', 'latex');

end