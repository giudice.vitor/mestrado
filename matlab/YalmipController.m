classdef YalmipController < handle

    properties
        type;
        T;
        zCoM;
        N;
        Nf;
        Nstep;
        maxVelocity;
        maxAcceleration;
        footDimensions;
        ySep;
        alpha;
        beta;
        gamma;
        delta;
        switchingStep;
    end
    
    methods
        function self = YalmipController(T, zCoM, N, Nf, Nstep,...
                vxmax, vymax, axmax, aymax, footDimensions, ySep,...
                alpha, beta, gamma, delta)
            self.type = 'humanoids';
            self.T = T;
            self.zCoM = zCoM;
            self.maxVelocity.vxmax = vxmax;
            self.maxVelocity.vymax = vymax;
            self.maxAcceleration.axmax = axmax;
            self.maxAcceleration.aymax = aymax;
            self.footDimensions = footDimensions;
            self.ySep = ySep;
            self.N = N;
            self.Nf = Nf;
            self.Nstep = Nstep;
            self.alpha = alpha;
            self.beta = beta;
            self.gamma = gamma;
            self.delta = delta;
        end
        
        function [d3X, d3Y, Xfb, Yfb, Xfaux, Yfaux, Bu] = control(self, k, x, y,...
                dXref, dYref, xc, yc, xa, ya, theta, isLeftSwing)
            if k > 5
                k = 5;
            end
            
            [dxsp, dysp, bsp] = YalmipOptimizationFactory.makeSupportPolygon(self.footDimensions, theta);
            [dxfr, dyfr, bfr] = YalmipOptimizationFactory.makeFootReachability(self.ySep, theta);
            [dxvl, dyvl, bvl] = YalmipOptimizationFactory.makeVelocityLimits(self.maxVelocity, theta);
            [dxa, dya, ba] = YalmipOptimizationFactory.makeAccelerationLimits(self.maxAcceleration, theta);
            
            prediction = Prediction(LIPMDynamics(self.T, self.zCoM), self.N);
            
            variables = YalmipOptimizationFactory.makeHumanoidsVariables(self.N, self.Nf);
            
            jerkCost = YalmipOptimizationFactory.makeJerkCost(variables, self.alpha);
            velTrackCost = YalmipOptimizationFactory.makeVelocityTrackingCost(variables, prediction,...
                dXref, dYref, x, y, self.beta);
            zmpTrackCost = YalmipOptimizationFactory.makeZMPTrackingCost(variables, prediction, x, y, self.gamma);
            stepDurCost = YalmipOptimizationFactory.makeStepDurationCost(variables, self.delta);
            
            cost = jerkCost + velTrackCost + zmpTrackCost + stepDurCost;
            
            integerConst = YalmipOptimizationFactory.makeIntegerConstraints(variables);
            auxVarConst = YalmipOptimizationFactory.makeAuxiliaryVariablesConstraints(variables);
            zmpErrorConst = YalmipOptimizationFactory.makeZMPErrorConstraints(variables, prediction,...
                dxsp, dysp, bsp, x, y);
            feetCollConst = YalmipOptimizationFactory.makeFeetCollisionConstraints(variables,...
                dxfr, dyfr, bfr, isLeftSwing);
            jointsConst = YalmipOptimizationFactory.makeJointsConstraints(variables,...
                dxvl, dyvl, bvl, xa, ya, self.T);
            currStepConst = YalmipOptimizationFactory.makeCurrentStepConstraints(variables,...
                xc, yc);
            maxAccelConst = YalmipOptimizationFactory.makeAccelerationConstraints(variables,...
                prediction, dxa, dya, ba, x, y);
            stepDurRefContConst = YalmipOptimizationFactory.makeStepDurationReferenceContinuousConstraints(variables,...
                self.Nstep, k);
            stepDurRefBinConst = YalmipOptimizationFactory.makeStepDurationReferenceBinaryConstraints(variables,...
                self.Nstep, k);
            boundConst = YalmipOptimizationFactory.makeBoundConstraints(variables);
            
            constraints = integerConst + auxVarConst + zmpErrorConst + feetCollConst +...
                jointsConst + currStepConst + maxAccelConst + stepDurRefContConst +...
                stepDurRefBinConst + boundConst;
            
%             options = sdpsettings('gurobi.NumericFocus', 3);
            optimize(constraints, cost);
            
            d3X = value(variables.d3X);
            d3Y = value(variables.d3Y);
            Xfb = value(variables.Xfb);
            Yfb = value(variables.Yfb);
            Xfaux = value(variables.Xfaux);
            Yfaux = value(variables.Yfaux);
            Bu = value(variables.Bu);
        end
        
        function numSteps = getNumSteps(self, k)
            numSteps = self.N;
        end
    end

end