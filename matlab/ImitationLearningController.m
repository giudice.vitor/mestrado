classdef ImitationLearningController < handle
    
    properties
        type;
        NeuralNetwork
        scaler
        T%
        N%
        Nstep%
    end
    
    methods
        function self = ImitationLearningController(T, N, Nstep, NeuralNetwork, scaler)%
            self.type = 'imitation';
            lgraph = layerGraph(NeuralNetwork);
            newlgraph = removeLayers(lgraph,'Dummy');
            newlgraph = connectLayers(newlgraph, 'concatenate_1', 'RegressionLayer_Dummy');
            NeuralNetwork = assembleNetwork(newlgraph);
            self.NeuralNetwork = NeuralNetwork;
            self.scaler = scaler;
            self.T = T;%
            self.N = N;%
            self.Nstep = Nstep;%
        end
        
        
        function [d3X, d3Y, Xf2, Yf2, Thetaf2, S1, feasible] = control(self, Kprime, X, Y,...
                  dX, dY, d2X, d2Y, dXr, dYr, dThetar, Xc, Yc, Thetac, Xa, Ya, Thetaa, Bls)
%             display(Kprime  + ", " +  X  + ", " + Y  + ", " +  dX  + ", " + dY + ", " + d2X + ", " + d2Y + ", " + dXr + ", " + dYr + ", " + dThetar + ", " + Xc + ", " + Yc + ", " + Thetac + ", " + Xa + ", " + Ya + ", " + Thetaa + ", " + Bls);
            [X, Y] = self.global_to_relative(X, Y, Xc, Yc, Thetac);
            [dX, dY] = self.rotate(dX, dY, -Thetac);
            [d2X, d2Y] = self.rotate(d2X, d2Y, -Thetac);

            [Xa, Ya] = self.global_to_relative(Xa, Ya, Xc, Yc, Thetac);
            Thetaa = Thetaa - Thetac;

            [dXr, dYr] = self.rotate(dXr, dYr, -Thetac);


            Y = self.invert_if_left_support(Y,Bls);
            dY = self.invert_if_left_support(dY,Bls);
            d2Y = self.invert_if_left_support(d2Y,Bls);
            Ya = self.invert_if_left_support(Ya,Bls);
            Thetaa = self.invert_if_left_support(Thetaa,Bls);
            dYr = self.invert_if_left_support(dYr,Bls);
            dThetar = self.invert_if_left_support(dThetar,Bls);

            
            X_train = [X, dX, d2X, Y, dY, d2Y, ...
                       Xa, Ya, Thetaa, dXr, dYr, dThetar, Kprime];
            X_train = self.normalize_input(X_train);
%             display(X_train);
            prediction = predict(self.NeuralNetwork, X_train);
%             display(prediction);
            prediction = self.unnormalize_output(prediction);
            
            d3X = prediction(1);
            d3Y = prediction(2);
            Xf2 = prediction(3);
            Yf2 = prediction(4);
            Thetaf2 = prediction(5);
            S1 = prediction(6);
            %feasible = prediction(7);
            
            
            d3Y = self.invert_if_left_support(d3Y, Bls);
            Yf2 = self.invert_if_left_support(Yf2, Bls);
            Thetaf2 = self.invert_if_left_support(Thetaf2, Bls);

            
            [d3X, d3Y] = self.rotate(d3X, d3Y, Thetac);
            [Xf2, Yf2] = self.back_to_global(Xf2, Yf2, Xc, Yc, Thetac);
            Thetaf2 = Thetaf2 + Thetac;
            
            Xf2 = [Xf2, Xf2];
            Yf2 = [Yf2, Yf2];
            Thetaf2 = [Thetaf2, Thetaf2];
            
            S1 = round(S1);
            display("kPrime:   " + Kprime + "      mStar:   " + S1);
        end
        
        
        function [x,y] = global_to_relative(self,x,y, x_origin, y_origin, theta_origin)        
            x = x - x_origin;
            y = y - y_origin;
            [x,y] = self.rotate(x,y, -theta_origin);      
        end


        function [x,y] = rotate(self,x,y,theta)     
            x_old = x;
            x = x.* cos(theta)     - y.*sin(theta);
            y = x_old.* sin(theta) + y.*cos(theta);        
        end


        function result = invert_if_left_support(self,vector,bls)
            result = bls.*vector + (bls-1).*vector;
        end

        
        function [x,y] = back_to_global(self,x,y,xc,yc,thetac)
            [x,y] = self.rotate(x,y, thetac);
            x = x + xc;
            y = y + yc;
        end
        
        
        function X_train = normalize_input(self, X_train)
            if self.scaler.input_scaler == "standard"
                X_train(1:12) = (X_train(1:12) - self.scaler.input_mean') ./ self.scaler.input_std';
                X_train(isnan(X_train)) = 0;
            else 
                X_train(1:12) = (X_train(1:12) - self.scaler.input_min') ./ (self.scaler.input_max' - self.scaler.input_min');
                X_train(isnan(X_train)) = 0;
            end
        end
        
        function Y_train = unnormalize_output(self, Y_train)
            if self.scaler.output_scaler == "standard"
                Y_train(1:5) = (Y_train(1:5)) .* self.scaler.output_std' + self.scaler.output_mean';
                Y_train(isnan(Y_train)) = 0;
            else 
                Y_train(1:5) = (Y_train(1:5)) .* (self.scaler.output_max' - self.scaler.output_min') + self.scaler.output_min';
                Y_train(isnan(Y_train)) = 0;
            end
            if self.scaler.version == "v1onehot"
                [~, index] = max(Y_train(6:end));
                aux = Y_train(6:end);
                aux(index) = 1;
                aux = round(aux);
                aux = aux * [1 2 3 4 5 6 7 8 9 10]';
                Y_train(6:end) = [];
                Y_train(6) = aux;
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        function numSteps = getNumSteps(self, k)
            numSteps = self.N;
        end
    end
end