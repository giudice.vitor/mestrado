import numpy as np


def global_to_relative(x, y, x_origin, y_origin, theta_origin):
    x = x - x_origin
    y = y - y_origin
    x, y = rotate(x, y, -theta_origin)
    return x, y


def rotate(x, y, theta):
    cossine = np.cos(theta)
    sine = np.sin(theta)
    x_old = x.copy()
    x = x * cossine - y * sine
    y = x_old * sine + y * cossine
    return x, y


def invert_if_left_support(vector, bls):
    result = bls * vector + (bls - 1.0) * vector
    return result
