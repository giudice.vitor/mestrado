#
#! this must be run on a TF2.X in order to save the model as a .pb file

# from keras.models import load_model

# logdir = '2022-01-08 01-20-08'
# model = load_model(logdir + '/imitationLearning', compile = False)
# model.save('savedModel');


#! this must be run on a TF1.X in order to save the model as a .pb file

import tensorflow as tf
from keras import backend as K
from keras.models import load_model
from tensorflow.python.framework import graph_util

K.set_learning_phase(0)
sess = K.get_session()
logdir = '2023-04-13 00-20-52'
model = load_model(logdir + '/imitationLearningDagger', compile = False)
orig_output_node_names = [node.op.name for node in model.outputs]
print(orig_output_node_names)

constant_graph = graph_util.convert_variables_to_constants(
            sess,
            sess.graph_def,
            orig_output_node_names)

tf.train.write_graph(constant_graph, logdir, 'imitationLearning.pb', as_text = False)
tf.train.write_graph(constant_graph, logdir, 'imitationLearningASC.pb', as_text = True)
