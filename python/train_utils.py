import os
from datetime import datetime

import tensorflow as tf
from keras import backend as K

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler, OneHotEncoder

from dataset import *
from draw import plot_history, debug_dataset


import json
from shutil import copyfile
from os import remove
from keras.models import Model
from keras.layers import Dense, Input, concatenate

FEASIBLE_LOSS_STRING = 'feasible_loss'

def process_dataset(X, Y, input_scaler=None, output_scaler=None):
    data = {}
    Y = np.array(Y)
    X = np.array(X)
    if input_scaler is not None:
        if input_scaler == 'standard':
            data['input_scaler'] = 'standard'
            data['input_mean'] = np.mean(X[:,0:12], axis=0).tolist()
            data['input_std'] = np.std(X[:,0:12], axis=0).tolist()

            X[:,0:12] = (X[:,0:12] - np.mean(X[:,0:12], axis=0)) / np.std(X[:,0:12], axis=0)
            X[:,0:12] = np.nan_to_num(X[:,0:12])
            
        
        if input_scaler == 'minmax':
            data['input_scaler'] = 'minmax'
            data['input_min'] = np.min(X[:,0:12], axis=0).tolist()
            data['input_max'] = np.max(X[:,0:12],axis=0).tolist()

            X[:,0:12] = (X[:,0:12] - np.min(X[:,0:12], axis=0)) / (np.max(X[:,0:12],axis=0) - np.min(X[:,0:12], axis=0))
            X [:,0:12]= np.nan_to_num(X[:,0:12])

    if output_scaler is not None:
        if output_scaler == 'standard':
            data['output_scaler'] = 'standard'
            data['output_mean'] = np.mean(Y[:,0:5], axis=0).tolist()
            data['output_std'] = np.std(Y[:,0:5], axis=0).tolist()

            Y[:,0:5] = (Y[:,0:5] - np.mean(Y[:,0:5], axis=0)) / np.std(Y[:,0:5], axis=0)
            Y[:,0:5] = np.nan_to_num(Y[:,0:5])
        
        if output_scaler == 'minmax':
            data['output_scaler'] = 'minmax'
            data['output_min'] = np.min(Y[:,0:5], axis=0).tolist()
            data['output_max'] = np.max(Y[:,0:5], axis=0).tolist()

            Y[:,0:5] = (Y[:,0:5] - np.min(Y[:,0:5], axis=0)) / (np.max(Y[:,0:5],axis=0) - np.min(Y[:,0:5], axis=0))
            Y[:,0:5] = np.nan_to_num(Y[:,0:5])

    with open('scaler.json', 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)

    return X, Y  

def log(nn, history, X_train, eval, dictionary):
    

    logdir = f'../{datetime.now().strftime("%Y-%m-%d %H-%M-%S")}'
    os.makedirs(logdir, exist_ok=True)
    plot_history(history, logdir, dictionary)

    nn.model.save(logdir + '/imitationLearning')
    inputs = Input(shape=(13,)) #! 14 with bds
    layer = nn.model(inputs)
    layer = concatenate(layer)
    outputs = Dense(1, activation='linear', name = 'Dummy')(layer)
    model = Model(inputs = inputs, outputs = outputs)
    model.compile(optimizer='Adam', loss='mean_squared_error')
    model.save(logdir + '/imitationLearningMatlab.h5')

    with open('scaler.json', 'r+', encoding='utf-8') as f:
        data = json.load(f)
        data.update(dictionary)
        data.update({'date': logdir})
        f.seek(0)
        json.dump(data, f, ensure_ascii=False, indent=4)

    copyfile('scaler.json', logdir + '/NN_Info.json')
    os.remove('scaler.json')

def build_dataset(version):
    if version == "v1":
        return build_dataset_v1()
    elif version == "v2":
        return build_dataset_v2()
    elif version == "v1onehot":
        return build_dataset_v1_onehotencoding()
    elif version == "v2onehot":
        return build_dataset_v2_onehotencoding()
    else:
        return None
        
def dataset(version, input_scaler, output_scaler):
    X_train, Y_train = build_dataset(version)
    X_train, Y_train = process_dataset(X_train, Y_train, input_scaler=input_scaler, output_scaler=output_scaler)

    assert( len(X_train) == len(Y_train) )	
    return  np.array(X_train), np.array(Y_train)

def divide_dataset(X, Y, test_split):
    test_split = float(test_split) / 100.0
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_split, shuffle=True)

    return X_train, X_test, Y_train, Y_test
