

python main.py --version=v2 --input-scaler=standard --output-scaler=standard --final-activation_regression=linear --final-activation_classification=linear --loss_regression=mae --loss_classification=mae --lr=0.00025 --activation=relu --test_split=10 --alpha_loss=0.15 --dropout_rate=0 --epochs=150 --batch_size=64 --nb_layers=4 --nb_neurons 1000 800 600 400



