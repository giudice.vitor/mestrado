import argparse, os
import numpy as np

def get_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("--input-scaler", default = None, help="Scaler for input data")
    parser.add_argument("--output-scaler", default = None, help="Scaler for output data" )
    parser.add_argument("--activation", required=True, help="Activation function for hidden layers. E.g linear, relu, tanh" )
    parser.add_argument("--final-activation_regression", required=True, help="Activation function for the regression layer. E.g linear, relu, tanh" )
    parser.add_argument("--final-activation_classification", required=True, help="Activation function for the classification layer. E.g linear, relu, tanh" )
    parser.add_argument("--version", required=True, help="Type of dataset" )
    parser.add_argument("--lr", default=0.001, help="Learning Rate", type = float)   
    parser.add_argument("--nb_layers", default=1, help="Number of hidden layers", type = int)
    parser.add_argument("--nb_neurons", default=64, help="Number of neurons in the hidden layers", nargs='*', type = int)    
    parser.add_argument("--epochs", default=1 , help="Number of epochs for training", type = int)
    parser.add_argument("--batch_size", default=32 , help="Batch size for mini-batch training", type = int)
    parser.add_argument("--test_split", default=10, help="Percentage of data reserved for evaluation", type = int)
    parser.add_argument("--loss_regression", required=True, help="Loss function for the regression output")
    parser.add_argument("--loss_classification", required=True, help="Loss function for the classification output")
    parser.add_argument("--alpha_loss", default = 1, help="Scaler of classification loss", type = float)
    parser.add_argument("--dropout_rate", default = 0.1, help="Scaler of classification loss", type = float)
    
    return parser

VALID_DATASET_CONFIGS = ["v1", "v2", "v1onehot", "v2onehot"]
VALID_SCALERS = ['standard', 'minmax']
VALID_LOSS = ['mse', 'mae', 'categorical_crossentropy', 'mape', 'sparse_categorical_crossentropy', 'binary_crossentropy', 'logcosh']

def is_float_try(str):
    try:
        float(str)
        return True
    except ValueError:
        return False

def validate_inputs(args):
    assert np.size(args.nb_neurons) == args.nb_layers, "Invalid number of neurons for the number of hidden layers"

    assert args.version in VALID_DATASET_CONFIGS, "Invalid dataset config **{}**".format(args.version)
    assert args.loss_regression in VALID_LOSS, "Invalid regression loss **{}**".format(args.loss)
    assert args.loss_classification in VALID_LOSS, "Invalid classification loss **{}**".format(args.loss)
    assert is_float_try(args.test_split), "Invalid test split **{}**".format(args.test_split)
    assert is_float_try(args.alpha_loss), "Invalid alpha loss **{}**".format(args.alpha_loss)

    if args.input_scaler != None:
        assert args.input_scaler in VALID_SCALERS, "Invalid input scaler **{}**".format(args.input_scaler)
    if args.output_scaler != None:
        assert args.output_scaler in VALID_SCALERS, "Invalid output scaler **{}**".format(args.output_scaler)

    

